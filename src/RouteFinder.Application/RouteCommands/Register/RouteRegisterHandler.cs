﻿using RouteFinder.Application.Configuration;
using RouteFinder.Application.Mappers;
using RouteFinder.Domain.Repositories;
using RouteFinder.Domain.RouteModel;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace RouteFinder.Application.RouteCommands.Register
{
    public interface IRouteRegisterHandler { }
    public class RouteRegisterHandler : ICommandHandler<RouteRegisterCommand, Response>, IRouteRegisterHandler
    {

        IRouteRepository _repo;

        public RouteRegisterHandler(IRouteRepository repository)
        {
            this._repo = repository;
        }

        public async Task<Response> Handle(RouteRegisterCommand request, CancellationToken cancellationToken)
        {
            var exist = await _repo.FindAsync(request.Origin, request.Destiny);
            if (exist != null)
            {
                throw new ArgumentException("Route already exists");
            }
            var route = RouteMapper.Mapper.Map<Route>(request);
            if (route.Destiny.Equals(route.Origin))
            {
                throw new ArgumentException("Origin and Destiny must be different");
            }
            await _repo.Add(route);
            return new Response(request.Id, "Route Saved");
        }
    }
}

﻿using RouteFinder.Application.Configuration;
using System.ComponentModel.DataAnnotations;

namespace RouteFinder.Application.RouteQueries.SearchBestRoute
{
    public class FindRouteQuery : IQuery<BestRouteResponse>
    {
        [Required]
        public string Origin { get; set; }
        [Required]
        public string Destiny { get; set; }

        public FindRouteQuery()
        {
        }

        public FindRouteQuery(string origin, string destiny)
        {
            Origin = origin;
            Destiny = destiny;
        }
    }
}

FROM mcr.microsoft.com/dotnet/nightly/sdk AS build-env
WORKDIR /app


COPY ./src ./
RUN dotnet restore RouteFinder.ConsoleApplication/RouteFinder.ConsoleApp.csproj

# Build da aplicacao

RUN dotnet publish -c Release -v minimal  -o out ./RouteFinder.ConsoleApplication/RouteFinder.ConsoleApp.csproj
# Build da imagem
FROM mcr.microsoft.com/dotnet/nightly/runtime AS netcore-runtime
WORKDIR /app
COPY --from=build-env /app/out .
COPY ./src/Sources/ .
ENTRYPOINT ["dotnet", "RouteFinder.ConsoleApp.dll"]

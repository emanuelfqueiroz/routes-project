﻿using System.Reflection;

namespace RouteFinder.Application.Configuration
{
    public class Assemblies
    {
        public static readonly Assembly Application = Assembly.GetExecutingAssembly();
    }
}

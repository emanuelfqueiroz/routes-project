﻿using Algorithms.ShortestPath.Dijkstra;
using RouteFinder.Application.RouteQueries.SearchBestRoute.Finders;
using RouteFinder.Domain.RouteModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RouteFinder.Application.RouteQueries.SearchBestRoute.Finder
{
    public class DijkstraAdapter : IFinderShortestPath
    {
        public BestRouteResponse Find(string from, string to, List<Route> routes)
        {
            try
            {
                Graph graph = ConvertToGraph(routes);
                var pathFinder = new PathFinder(graph);

                var path = pathFinder.FindShortestPath(
                   graph.Nodes.Single(node => node.Id == from),
                   graph.Nodes.Single(node => node.Id == to));

                return new BestRouteResponse()
                {
                    Origin = from,
                    Destiny = to,
                    Routes = path.Segments.Select<PathSegment, Route>(p => Map(p)).ToList()
                };
            }
            catch (NoPathFoundException e)
            {
                return null;
            }
            catch (Exception exc)
            {
                if (exc.Message.Equals("Sequence contains no matching element", StringComparison.InvariantCultureIgnoreCase))
                {
                    return null;
                }
                throw exc;
            }
        }

        private static Graph ConvertToGraph(List<Route> routes)
        {
            var builder = new GraphBuilder();

            var allLocations = routes.SelectMany<Route, string>(r =>
                new string[] { r.Origin, r.Destiny }
            ).Distinct();
            foreach (var location in allLocations)
            {
                builder.AddNode(location);
            }

            foreach (var route in routes)
            {
                builder.AddLink(
                    sourceId: route.Origin,
                    destinationId: route.Destiny,
                    weight: Convert.ToDouble(route.Cost));
            }

            var graph = builder.Build();
            return graph;
        }

        public Route Map(PathSegment p)
        {
            return new Route()
            {
                Cost = Convert.ToDecimal(p.Weight),
                Origin = p.Origin.Id,
                Destiny = p.Destination.Id
            };
        }
    }
}

﻿using System.IO;

namespace RouteFinder.Application.Tests.Common
{
    public class Helper
    {
        public static void RecreateFile(string filename)
        {
            if (File.Exists(filename))
            {
                File.Delete(filename);
            }
            using (File.Create(filename)) ;
        }
    }
}

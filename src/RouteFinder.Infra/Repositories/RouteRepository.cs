﻿using CsvHelper;
using RouteFinder.Domain.Repositories;
using RouteFinder.Domain.RouteModel;
using RouteFinder.Infra.Repositories.Data;
using RouteFinder.Infra.Repositories.Mappers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace RouteFinder.Infra.Repositories
{
    public class RouteRepository : IRouteRepository
    {
        public string Path => Environment.GetEnvironmentVariable("routes:path");

        public RouteRepository()
        {

        }

        public async Task<List<Route>> GetAllRouteAsync()
        {
            return await GetAllRouteFromPathAsync(Path);
        }

        public async Task<List<Route>> GetAllRouteFromPathAsync(string filePath)
        {
            using (var reader = new StreamReader(filePath))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csv.Configuration.HasHeaderRecord = false;
                var records = csv.GetRecords<RouteCsv>();
                return await Task.FromResult(records.ToList().Select(p => p.Map()).ToList());
            }
        }

        public async Task Add(Route route)
        {
            using (var writer = new StreamWriter(Path, append: true))
            using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
            {
                csv.Configuration.HasHeaderRecord = false;
                var record = route.Map();
                csv.WriteRecord<RouteCsv>(record);
                csv.NextRecord();
                await Task.CompletedTask;
            }
        }

        public async Task<Route> FindAsync(string from, string to)
        {
            var list = await GetAllRouteAsync();
            return list.FirstOrDefault(r =>
            {
                return r.Origin.Equals(from, StringComparison.InvariantCultureIgnoreCase)
                    && r.Destiny.Equals(to, StringComparison.InvariantCultureIgnoreCase);
            });
        }
    }
}

﻿using RouteFinder.Application.Configuration;
using System.ComponentModel.DataAnnotations;

namespace RouteFinder.Application.RouteCommands.Register
{
    public class RouteRegisterCommand : CommandBase<Response>
    {
        private string origin;
        private string destiny;

        [Required(AllowEmptyStrings = false)]
        public string Origin { get => origin?.ToUpper(); set => origin = value; }

        [Required(AllowEmptyStrings = false)]
        public string Destiny { get => destiny?.ToUpper(); set => destiny = value; }

        [Required(AllowEmptyStrings = false)]
        [Range(0, 999999)]
        public decimal Cost { get; set; }
    }
}

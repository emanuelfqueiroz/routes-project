﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using RouteFinder.Application.RouteCommands.Register;
using RouteFinder.Application.RouteQueries.List;
using RouteFinder.Application.RouteQueries.SearchBestRoute;
using System.Net;
using System.Threading.Tasks;

namespace RouteFinder._WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RouteController : ControllerBase
    {
        private readonly IMediator _mediator;

        public RouteController(IMediator mediator)
        {
            this._mediator = mediator;
        }
        /// <summary>
        /// Create a new Order
        /// </summary>
        [HttpPost]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create([FromBody] RouteRegisterCommand registerCommand)
        {
            return Ok(await _mediator.Send(registerCommand));
        }

        [HttpGet("ListRoutes")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get()
        {
            return Ok(await _mediator.Send(new RouteQuery()));
        }

        [HttpPost("FindBestRoute")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult<BestRouteResponse>> FindBestRoute(
            [FromBody] FindRouteQuery query)
        {
            return Ok(await _mediator.Send(query));
        }
    }
}
﻿using RouteFinder.Application.Configuration;
using RouteFinder.Application.RouteQueries.SearchBestRoute.Finders;
using RouteFinder.Domain.Repositories;
using System.Threading;
using System.Threading.Tasks;

namespace RouteFinder.Application.RouteQueries.SearchBestRoute
{
    public class FindBestRouteHandler : IQueryHandler<FindRouteQuery, BestRouteResponse>
    {
        public IRouteRepository _repo { get; set; }
        public IFinderShortestPath _finder { get; set; }

        public FindBestRouteHandler(IRouteRepository repo, IFinderShortestPath finder)
        {
            _repo = repo;
            _finder = finder;
        }

        public async Task<BestRouteResponse> Handle(FindRouteQuery request, CancellationToken cancellationToken)
        {
            var routes = await _repo.GetAllRouteAsync();

            return _finder.Find(request.Origin, request.Destiny, routes);

        }
    }
}

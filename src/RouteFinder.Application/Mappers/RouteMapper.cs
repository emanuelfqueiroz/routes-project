﻿using AutoMapper;
using RouteFinder.Application.RouteCommands.Register;
using RouteFinder.Application.RouteQueries.List;
using RouteFinder.Domain.RouteModel;

namespace RouteFinder.Application.Mappers
{
    public static class RouteMapper
    {
        public static IMapper Mapper;
        static RouteMapper()
        {
            Mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<RouteDTO, Route>().ReverseMap();
                cfg.CreateMap<RouteRegisterCommand, Route>().ReverseMap();
            }).CreateMapper();
        }
        public static Route Map(this RouteDTO route)
        {
            return Mapper.Map<Route>(route);
        }
        public static RouteDTO Map(this Route route)
        {
            return Mapper.Map<RouteDTO>(route);
        }
    }
}

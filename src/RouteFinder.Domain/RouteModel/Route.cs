﻿namespace RouteFinder.Domain.RouteModel
{
    public class Route
    {
        public string Origin { get; set; }
        public string Destiny { get; set; }
        public decimal Cost { get; set; }
    }
}

﻿using CsvHelper.Configuration.Attributes;

namespace RouteFinder.Infra.Repositories.Data
{
    public class RouteCsv
    {
        [Index(0)]
        public string Origin { get; set; }
        [Index(1)]
        public string Destiny { get; set; }
        [Index(2)]
        public decimal Cost { get; set; }


    }
}

﻿using RouteFinder.Domain.RouteModel;
using System.Collections.Generic;
using System.Linq;

namespace RouteFinder.Application.RouteQueries.SearchBestRoute
{
    public class BestRouteResponse
    {
        public string Origin { get; set; }
        public string Destiny { get; set; }
        public List<Route> Routes { private get; set; }
        public List<string> Path => Routes.Select(r => r.Origin)
            .Union(new string[] { Routes.Last().Destiny })
            .ToList();
        public decimal TotalCost => Routes.Sum(p => p.Cost);
    }
}
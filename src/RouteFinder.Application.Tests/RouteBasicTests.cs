using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using RouteFinder.Application.Configuration;
using RouteFinder.Application.RouteCommands.Register;
using RouteFinder.Application.RouteQueries.List;
using RouteFinder.Application.Tests.Common;
using RouteFinder.Infra.DI;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RouteFinder.Application.Tests
{
    public class RouteBasicTests
    {
        const string FILE = "./RouteRegisterTest.csv";
        private ServiceProvider provider;

        [SetUp]
        public void Setup()
        {
            Helper.RecreateFile(FILE);
            provider = Loader.BasicRegister(FILE);
        }

        [Test]
        public async Task RegisterAndRead()
        {
            var cancelToken = new System.Threading.CancellationToken();
            var commands = new List<RouteRegisterCommand>();

            Enumerable.Range(1, 100).ToList().ForEach(i =>
            {
                commands.Add(new RouteRegisterCommand()
                {
                    Origin = "OR" + i,
                    Destiny = "AA" + i,
                    Cost = i
                });
            });

            var register = provider.GetService<ICommandHandler<RouteRegisterCommand, Response>>();
            foreach (var command in commands)
            {
                await register.Handle(command, cancelToken);
            }

            Assert.IsTrue(true, "Register Tested");

            var query = provider.GetService<IQueryHandler<RouteQuery, ResponseList<RouteDTO>>>();
            var response = await query.Handle(new RouteQuery(), cancelToken);
            var routes = response.Result;
            Assert.AreEqual(commands.Count, routes.Count, "Routes registered");


            foreach (var c in commands)
            {
                var route = routes.First(r => r.Origin == c.Origin && r.Destiny == c.Destiny && r.Cost == c.Cost);
                if (route is null)
                {
                    Assert.Fail($"Command {c.Origin} not found");
                    return;
                }
            }

            Assert.True(true, "All routes exists");
        }


    }
}
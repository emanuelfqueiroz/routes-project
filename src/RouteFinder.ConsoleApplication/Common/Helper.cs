﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RouteFinder.ConsoleApp.Common
{
    public static class Helper
    {
        public static (bool isValid, List<ValidationResult> result) Validate<T>(T t)
        {
            var context = new ValidationContext(t, serviceProvider: null, items: null);
            var results = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(t, context, results);

            return (isValid, results);
        }
    }
}

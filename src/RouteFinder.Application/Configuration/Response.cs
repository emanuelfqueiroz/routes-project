﻿using System;
using System.Collections.Generic;

namespace RouteFinder.Application.Configuration
{

    public class Response<T> : Response
    {
        public T Result { get; set; }
    }
    public class ResponseList<T> : Response
    {
        public List<T> Result { get; set; }
    }
    public class Response
    {
        public string Message { get; set; } = null;
        public string RequestId { get; set; } = null;

        public Response()
        {
        }

        public Response(Guid RequestId, string message) : this(RequestId.ToString(), message) { }

        public Response(string requestId, string message) =>
            (RequestId, Message) = (requestId, message);
    }
}

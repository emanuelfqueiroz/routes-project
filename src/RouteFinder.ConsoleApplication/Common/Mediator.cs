﻿using RouteFinder.Application.Configuration;
using RouteFinder.Application.RouteQueries.SearchBestRoute;
using RouteFinder.Infra.DI;
using System.Threading.Tasks;

namespace RouteFinder.ConsoleApp.Common
{
    public class Mediator
    {
        public Loader ServiceProvider { get; set; }

        public Mediator(Loader serviceProvider)
        {
            ServiceProvider = new Loader();
        }

        public async Task<TResult> QueryHandler<TQuery, TResult>(TQuery query) where TQuery : IQuery<TResult>
        {
            var handler = ServiceProvider.GetService<IQueryHandler<TQuery, TResult>>();
            return  await handler.Handle(query, new System.Threading.CancellationToken());
        }

    }
}

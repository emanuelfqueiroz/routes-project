﻿using RouteFinder.Domain.RouteModel;
using System.Collections.Generic;

namespace RouteFinder.Application.RouteQueries.SearchBestRoute.Finders
{
    public interface IFinderShortestPath
    {
        BestRouteResponse Find(string From, string To, List<Route> routes);
    }
}

﻿namespace RouteFinder.Application.RouteQueries.List
{
    public class RouteDTO
    {
        public string Origin { get; set; }
        public string Destiny { get; set; }
        public decimal Cost { get; set; }
    }
}

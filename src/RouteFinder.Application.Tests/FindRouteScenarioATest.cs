﻿using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using RouteFinder.Application.Configuration;
using RouteFinder.Application.RouteQueries.SearchBestRoute;
using RouteFinder.Domain.RouteModel;
using RouteFinder.Infra.DI;
using RouteFinder.Infra.Repositories;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace RouteFinder.Application.Tests
{
    class FindRouteScenarioATest
    {
        const string FILE = "./Sources/Routes.csv";
        const string SOLUTIONS = "./Sources/Solution.csv";

        private ServiceProvider provider;
        private CancellationToken cancelToken;
        private IQueryHandler<FindRouteQuery, BestRouteResponse> queryHander;

        [SetUp]
        public async Task Setup()
        {
            provider = Loader.BasicRegister(FILE);
            cancelToken = new System.Threading.CancellationToken();
            queryHander = provider.GetService<IQueryHandler<FindRouteQuery, BestRouteResponse>>();
        }

        [Test]
        public async Task CheckScenarios()
        {
            var solutions = await GetSolutions();
            foreach (var slv in solutions)
            {
                var response = await Find(slv.Origin, slv.Destiny);
                Assert.AreEqual(
                    slv.Cost,
                    response.TotalCost,
                    message: $" {slv.Origin}-{slv.Destiny}"
                );
            }
        }

        private async Task<BestRouteResponse> Find(string from, string to)
        {
            return await queryHander.Handle(new FindRouteQuery(from, to), cancelToken);
        }
        private async Task<List<Route>> GetSolutions()
        {
            var repo = new RouteRepository();
            return await repo.GetAllRouteFromPathAsync(SOLUTIONS);
        }
    }
}

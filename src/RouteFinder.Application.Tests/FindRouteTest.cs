﻿using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using RouteFinder.Application.Configuration;
using RouteFinder.Application.RouteCommands.Register;
using RouteFinder.Application.RouteQueries.SearchBestRoute;
using RouteFinder.Application.Tests.Common;
using RouteFinder.Infra.DI;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RouteFinder.Application.Tests
{
    public class FindRouteTest
    {
        const string FILE = "./FindTest.csv";
        private ServiceProvider provider;
        private IQueryHandler<FindRouteQuery, BestRouteResponse> queryHander;

        [SetUp]
        public async Task Setup()
        {
            Helper.RecreateFile(FILE);
            provider = Loader.BasicRegister(FILE);

            var register = provider.GetService<ICommandHandler<RouteRegisterCommand, Response>>();
            var cancelToken = new System.Threading.CancellationToken();

            var commands = new List<RouteRegisterCommand>()
            {
                new RouteRegisterCommand(){Cost = 10, Origin = "A", Destiny = "B"},
                new RouteRegisterCommand(){Cost = 7, Origin = "B", Destiny = "C"}
            };

            foreach (var command in commands)
            {
                await register.Handle(command, cancelToken);
            }

            queryHander = provider.GetService<IQueryHandler<FindRouteQuery, BestRouteResponse>>();
        }

        [Test]
        public async Task A_C_Test()
        {
            var cancelToken = new System.Threading.CancellationToken();
            var response = await queryHander.Handle(
                new FindRouteQuery() { Origin = "A", Destiny = "C" }, cancelToken);
            Assert.AreEqual(17, response.TotalCost);
        }
        [Test]
        public async Task Path_Test()
        {
            var cancelToken = new System.Threading.CancellationToken();
            var response = await queryHander.Handle(
                new FindRouteQuery() { Origin = "A", Destiny = "C" }, cancelToken);

            Assert.IsTrue(response.Path[0].Equals("A"));
            Assert.IsTrue(response.Path[1].Equals("B"));
            Assert.IsTrue(response.Path[2].Equals("C"));
        }
        [Test]
        public async Task A_C_No_Reverse_Test()
        {
            var cancelToken = new System.Threading.CancellationToken();
            var response = await queryHander.Handle(
                new FindRouteQuery() { Origin = "C", Destiny = "A" }, cancelToken);
            Assert.IsNull(response);
        }

    }
}

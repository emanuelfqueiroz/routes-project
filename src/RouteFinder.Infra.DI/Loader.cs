﻿using Microsoft.Extensions.DependencyInjection;
using RouteFinder.Application.Configuration;
using RouteFinder.Application.RouteCommands.Register;
using RouteFinder.Application.RouteQueries.List;
using RouteFinder.Application.RouteQueries.SearchBestRoute;
using RouteFinder.Application.RouteQueries.SearchBestRoute.Finder;
using RouteFinder.Application.RouteQueries.SearchBestRoute.Finders;
using RouteFinder.Domain.Repositories;
using RouteFinder.Infra.Repositories;
using System;
using System.Reflection;

namespace RouteFinder.Infra.DI
{
    public class Loader
    {

        public static void Register(IServiceCollection services, Action<Assembly> setMediator)
        {
            foreach (var item in GetAll())
            {
                setMediator(item);
            }
            services.AddSingleton<IRouteRepository, RouteRepository>();
            services.AddSingleton<IFinderShortestPath, DijkstraAdapter>();
            services.AddSingleton<ICommandHandler<RouteRegisterCommand, Response>, RouteRegisterHandler>();
            services.AddSingleton<IQueryHandler<FindRouteQuery, BestRouteResponse>, FindBestRouteHandler>();
            services.AddSingleton<IQueryHandler<RouteQuery, ResponseList<RouteDTO>>, RouteQueryHandler>();
        }

        public static ServiceProvider providerDI; 
        public static ServiceProvider BasicRegister(string filePath)
        {
            Environment.SetEnvironmentVariable("routes:path", filePath);
            var services = new ServiceCollection();
            Register(services, p => { });
            providerDI =  services.BuildServiceProvider();
            return providerDI;
        }


        public static Assembly[] GetAll()
        {
            return new[] {
                typeof(Application.Configuration.ICommand).GetTypeInfo().Assembly,
                typeof(Application.Configuration.IQuery<Response>).GetTypeInfo().Assembly
            };
        }

        public  T GetService<T>()
        {
            return providerDI.GetService<T>();
        }
    }
}

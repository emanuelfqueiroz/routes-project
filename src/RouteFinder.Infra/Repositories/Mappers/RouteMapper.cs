﻿using AutoMapper;
using RouteFinder.Domain.RouteModel;
using RouteFinder.Infra.Repositories.Data;

namespace RouteFinder.Infra.Repositories.Mappers
{
    public static class RouteMapper
    {
        static IMapper mapper;
        static RouteMapper()
        {
            mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<RouteCsv, Route>().ReverseMap();
            }).CreateMapper();
        }
        public static Route Map(this RouteCsv route)
        {
            return mapper.Map<Route>(route);
        }
        public static RouteCsv Map(this Route route)
        {
            return mapper.Map<RouteCsv>(route);
        }
    }
}

﻿
using RouteFinder.Application.RouteQueries.SearchBestRoute;
using RouteFinder.ConsoleApp;
using RouteFinder.ConsoleApp.Common;
using RouteFinder.Infra.DI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace RouteFinder.ConsoleApplication
{
    
    class Program
    {
        static Mediator mediator;
        static int safeWhile = 100;
        static async Task Main(string[] args)
        {
            try
            {
                Console.Title = "Route Finder";

                if (args.Count() <= 0)
                {
                    Print("No 'filepath' parameter was not found.");
                }
                else
                {
                    Loader.BasicRegister(filePath: args[0]);
                    Environment.SetEnvironmentVariable("routes:path", args[0]);
                    mediator = new Mediator(new Loader());

                    do
                    {
                        await Run();
                        Print("\nPress Ctrl+C or Ctrl+Break to finish" +
                            "\nPress ENTER to try again");
                        WaitNextStep();

                    } while (--safeWhile > 0);
                    Console.Clear();
                }
            }
            catch (Exception exc)
            {
                Print(exc.Message);
            }


            Environment.Exit(1);
            Console.ReadLine();
        }

        private static void WaitNextStep()
        {
            Console.ReadLine();
            Console.Clear();
        }


        private static async Task Run()
        {
            try
            {
                Print(" *** Welcome to Route Finder **");

                Print("\nPlease enter the route: ");
                CleanKeys();
                var input = Console.ReadLine();

                var query = Interpreter.FindRouteQuery(input);
                var validation = Helper.Validate<FindRouteQuery>(query);

                if (validation.isValid)
                {
                    var response = await mediator.QueryHandler<FindRouteQuery, BestRouteResponse>(query);
                    Print("\nBest Route: ");
                    Print(response);
                }
                else
                {
                    Print(validation.result);
                }
            }
            catch (Exception exc)
            {
                Print(exc.Message);
            }
        }

        private static void CleanKeys()
        {
            while (Console.KeyAvailable)
            {
                Console.ReadKey(false);
            }
        }

        #region Printers
        private static void Print(List<ValidationResult> result)
        {
            result.ForEach(err => Print(err.ErrorMessage + "\n"));
        }
        private static void Print(string text)
        {
            Console.WriteLine(text);
        }
        private static void Print(BestRouteResponse response)
        {
            if (response is null)
            {
                Print("Route not found");
                return;
            }
            Print(String.Join(" - ", response.Path));
            Print("Cost: " + response.TotalCost);
        }
        #endregion
    }
}

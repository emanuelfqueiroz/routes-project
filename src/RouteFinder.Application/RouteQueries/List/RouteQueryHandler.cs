﻿using RouteFinder.Application.Configuration;
using RouteFinder.Application.Mappers;
using RouteFinder.Domain.Repositories;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RouteFinder.Application.RouteQueries.List
{
    public class RouteQueryHandler : IQueryHandler<RouteQuery, ResponseList<RouteDTO>>
    {
        public IRouteRepository Repository { get; set; }

        public RouteQueryHandler(IRouteRepository repository)
        {
            Repository = repository;
        }

        public async Task<ResponseList<RouteDTO>> Handle(RouteQuery request, CancellationToken cancellationToken)
        {
            var routes = await Repository.GetAllRouteAsync();
            var dtoRoutes = routes.Select(p => p.Map());

            return new ResponseList<RouteDTO>()
            {
                Result = dtoRoutes.ToList()
            };
        }
    }
}

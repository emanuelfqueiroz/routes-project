﻿using RouteFinder.Application.RouteQueries.SearchBestRoute;
using System;
using System.Linq;

namespace RouteFinder.ConsoleApp
{
    public class Interpreter
    {
        const string SPLIT = "-";
        public static FindRouteQuery FindRouteQuery(string input)
        {
            input = input.ToUpper();
            if (!input.Contains(SPLIT))
            {
                throw new ArgumentException("The command is not valid. Please use the following format: GRU-CDG");
            }
            var locations = input.Split(SPLIT);
            return new FindRouteQuery()
            {
                Origin = locations.First(),
                Destiny = locations.Last()
            };
        }
    }
}

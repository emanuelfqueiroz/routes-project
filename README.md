# ROUTE FINDER 

### Emanuel Feliciano Queiroz

Permite encontrar a rota de menor custo. 
Interfaces: Console e Web API

### Overview

#### Tecnologias

- .NET Core 3.1: Web API
- .NET 2 Standard: Bibliotecas e Camadas
- .NET 5: ConsoleApp
- .NET 5 + NUnit: Projeto de Testes

#### Arquitetura

- CQRS - Segrega a aplicação por responsabilidades
- Testes - Realiza testes `eficientes` utilizando a camada de aplicação como referência.

#### Solução

O desafio está relacionado a um problema conhecido na computação como `"shortest path problem"` : <https://en.wikipedia.org/wiki/Shortest_path_problem> 

Decidi utilizar o algortimo de `Dijkstra` devido seu excelente resultado e ser amplamente conhecido [`manutenção do sistema`].

![Opcoes](https://gitlab.com/emanuelfqueiroz/routes-project/-/raw/master/doc/images//algoritmos.png)

## Como executar a aplicação

### Docker

na raiz do projeto 
> docker-compose up -d

Os arquivos serão copiados do diretório
**/src/Sources** serão copiados para o container

#### Docker Web API:
acesse : http://localhost:8080/swagger/index.html

para alterar o arquivo, basta alterar no web.Dockerfile:
>  ENV routes:path ./input-file.csv

#### Docker Console
  
> docker run -it routefinder/console **$filePath** /bin/bash

> docker run -it routefinder/console ./input-file.csv /bin/bash

## Estrutura dos arquivos/pacotes

![Comparação](https://gitlab.com/emanuelfqueiroz/routes-project/-/raw/master/doc/images//project_struture.png)
  
- **Algorithms.ShortestPath**:  possui  o um projeto construido para ser uma `biblioteca/library`. 
  - Algoritmo independente, por isso o diretório *RouteFinder.Artifacts*
  
- **RouteFinder._WebAPI** possui as APIs do projecto
  - swagger para documentação
  - `MediatR` mantem os controllers simples, *livres* e focados em requisições HTTP

       ![mediator](https://gitlab.com/emanuelfqueiroz/routes-project/-/raw/master/doc/images//mediator.png)

- **RouteFinder._Console**: interface console

- **RouteFinder.Application** camada de Aplicação
  
  - Serviços organizados entre queries e commands
  - Par encontrar a rota de menor custo, a aplicação consome a interface `IFinderShortestPath`
    - Permite a alteração estática ou dinâmica de algoritmos
    - Princípio SOLID
    - O adaptador `DijkstraAdapter` segue a interface *IFinderShortestPath* e garante que os algoritmos serão independentes para outros consumidores;

- **RouteFinder.Application.Tests**
  - testes focados no negócio
  - acessível à equipe de QA
    - os cenários podem ser criados uilizando os arquivos `Routes.csv` e `solutions.csv`
    ![tests](https://gitlab.com/emanuelfqueiroz/routes-project/-/raw/master/doc/images//graph_for_tests.png)

- **RouteFinder.Domain** Classes de domínio
- **RouteFinder.Infra** Classes de Infraestrutura
- **RouteFinder.Infra.DI**  a injeção de dependências é um projeto próprio devido a utilização extensiva
  
### Notas

- *As decisões de design foram descritas nos tópicos anteriores
  
- **Importante** cursor do arquivo CSV sempre deve estar na primeira posição

<pre>
    ABC,DEF,100
    | <------ cursor
</pre>


### API Rest
os detalhes poderão ser consultados na página inicial da web api `/swagger/index.html`

![Swagger](https://gitlab.com/emanuelfqueiroz/routes-project/-/raw/master/doc/images//webAPI_overview.png)


## Solução Alternativa
utilizar um banco de dados em grafo para consolidação desses dados. Geralmente, já possuem a operação `shortest path`.
Ex:Neo4j, SQL Server Graph:
<https://docs.microsoft.com/en-us/sql/relational-databases/graphs/sql-graph-shortest-path?view=sql-server-ver15>

![Swagger](https://gitlab.com/emanuelfqueiroz/routes-project/-/raw/master/doc/images//alternativa.png)


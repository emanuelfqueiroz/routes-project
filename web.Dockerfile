FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env
WORKDIR /app

COPY ./src ./
RUN dotnet restore RouteFinder._WebAPI/RouteFinder._WebAPI.csproj

# Build da aplicacao

RUN dotnet publish -c Release -v minimal -o out ./RouteFinder._WebAPI/RouteFinder._WebAPI.csproj
# Build da imagem
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS aspnetcore-runtime
WORKDIR /app
COPY --from=build-env /app/out .
COPY ./src/Sources .
ENV routes:path ./input-file.csv
ENTRYPOINT ["dotnet", "RouteFinder._WebAPI.dll"]
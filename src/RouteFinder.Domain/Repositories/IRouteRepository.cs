﻿using RouteFinder.Domain.RouteModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RouteFinder.Domain.Repositories
{
    public interface IRouteRepository
    {
        Task Add(Route route);
        Task<List<Route>> GetAllRouteAsync();
        Task<Route> FindAsync(string from, string to);
    }
}
